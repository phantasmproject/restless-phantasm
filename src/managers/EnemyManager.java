package managers;

import java.awt.Graphics2D;
import java.util.ArrayList;

import entities.Enemy;
import entities.VisualEffect;
import main.GamePanel;
import utils.Animation;
import utils.Util;

public class EnemyManager {
	private static final int ENEMY_X = 850;
	private static final int ENEMY_Y = 50;
	
	private static final String ENEMY0_BASE_RES = "/enemies/archon/archon";
	public static final String ENEMY0_RES = "/enemies/archon/archon1.png";
	private static final int ENEMY0_RES_COUNT = 3;
	
	private static final String ENEMY1_BASE_RES = "/enemies/dorver/dorver-idle";
	public static final String ENEMY1_RES = "/enemies/dorver/dorver-idle1.png";
	private static final String ENEMY_RES_EXT = ".png";
	private static final int ENEMY1_RES_COUNT = 4;
	
	private static final String FLAME_BASE_RES = "/visual_effects/magic_flames/flame";
	private static final int FLAME_RES_COUNT = 50;
	private static final String FLAME_RES_EXT = ".png";
	
	private static final String EXPLOSION_BASE_RES =
		"/visual_effects/explosions/yellow_explosion/explosion";
	private static final int EXPLOSION_RES_COUNT = 10;
	private static final String EXPLOSION_RES_EXT = ".png";
	
	private static final int ENGAGE_ENEMY_DEST_X =
		GamePanel.WIDTH / 2;
	private static final int ENGAGE_ENEMY_DEST_Y =
		GamePanel.HEIGHT / 4;
	private static final int ENGAGE_FLIPPED_ENEMY_DEST_X =
		GamePanel.WIDTH - (GamePanel.WIDTH / 6);
	private static final int ENGAGE_FLIPPED_ENEMY_DEST_Y =
		GamePanel.HEIGHT / 4;
	
	private Enemy currentEnemy;
	
	public boolean isFlamed;
	private boolean isExploded;
	private VisualEffect flame;
	private VisualEffect explosion;
	
	private int currentEnemiesHp;
	private ArrayList<Enemy> enemies;
	
	public EnemyManager() {
		currentEnemiesHp = 3;
		enemies = new ArrayList<Enemy>();
		isFlamed = false;
		isExploded = false;
		flame = new VisualEffect
			(FLAME_BASE_RES, FLAME_RES_EXT,
				FLAME_RES_COUNT, 1);
		explosion = new VisualEffect
			(EXPLOSION_BASE_RES, EXPLOSION_RES_EXT,
				EXPLOSION_RES_COUNT, 2);
		
		init();
	}
	
	public void init() {
		Enemy enemy = new Enemy(ENEMY0_RES, currentEnemiesHp);
		enemy.initAnimation
			(ENEMY0_BASE_RES, ENEMY_RES_EXT, ENEMY0_RES_COUNT, 6);
		enemy.setScale(2.5);
		enemies.add(enemy);
		
		enemy = new Enemy(ENEMY1_RES, currentEnemiesHp);
		enemy.initAnimation
			(ENEMY1_BASE_RES, ENEMY_RES_EXT, ENEMY1_RES_COUNT, 3);
		enemy.setScale(2.5);
		enemies.add(enemy);
		
		setEnemiesPos();
			
		// Magic numbers, because I don't care anymore.
		flame.setPos(ENGAGE_ENEMY_DEST_X + 40, ENGAGE_ENEMY_DEST_Y - 50);
		
		explosion.setScale(1.5);

		// Magic numbers, because I don't care anymore.
		explosion.setPos(ENGAGE_ENEMY_DEST_X - 100, ENGAGE_ENEMY_DEST_Y - 200);
	}
	
	public Enemy randomizeEnemy() {
		int i = Util.randNumInRange(0, enemies.size() - 1);
		
		currentEnemy = enemies.get(i);
		
		return currentEnemy;
	}
	
	public Enemy getCurrentEnemy() {
		return currentEnemy;
	}

	private static final int DEST_MARGIN = 5;
	
	public void dragEnemyToSpecificPosition() {
		if (currentEnemy == null) return;

		int newDX, newDY;
		int engageEnemyDestX = (currentEnemy.isFlipped()) ?
			ENGAGE_FLIPPED_ENEMY_DEST_X : ENGAGE_ENEMY_DEST_X;
		int engageEnemyDestY = (currentEnemy.isFlipped()) ?
			ENGAGE_FLIPPED_ENEMY_DEST_Y : ENGAGE_ENEMY_DEST_Y;
		
		if (currentEnemy.getX() != engageEnemyDestX)
			newDX = (currentEnemy.getX() < engageEnemyDestX) ? 5 : -5;
		else
			newDX = 0;
		
		if (currentEnemy.getY() != engageEnemyDestY)
			newDY = (currentEnemy.getY() < engageEnemyDestY) ? 5 : -5;
		else
			newDY = 0;
		
		currentEnemy.setVelocity(newDX, newDY);
	}
	
	public void adjustEnemyPosIfWithinRangeFromDest() {
		int engageEnemyDestX = (currentEnemy.isFlipped()) ?
			ENGAGE_FLIPPED_ENEMY_DEST_X : ENGAGE_ENEMY_DEST_X;
		int engageEnemyDestY = (currentEnemy.isFlipped()) ?
			ENGAGE_FLIPPED_ENEMY_DEST_Y : ENGAGE_ENEMY_DEST_Y;
		
		if (currentEnemy.getX() >= (engageEnemyDestX - DEST_MARGIN) 
			&& currentEnemy.getX() <= (engageEnemyDestX + DEST_MARGIN)) {
			
			currentEnemy.setVelocity(0, currentEnemy.getDY());
			currentEnemy.setPos(engageEnemyDestX, currentEnemy.getY());
		}
		
		if (currentEnemy.getY() >= (engageEnemyDestY - DEST_MARGIN) 
			&& currentEnemy.getY() <= (engageEnemyDestY + DEST_MARGIN)) {
			
			currentEnemy.setVelocity(currentEnemy.getDX(), 0);
			currentEnemy.setPos(currentEnemy.getX(), engageEnemyDestY);
		}
	}
	
	public boolean updateEnemy() {
		if (isExploded) {
			explosion.update();
			
			if (getExplosionAnimation().getCurrentFrameIndex()
				>= EXPLOSION_RES_COUNT - 1) {
				isExploded = false;

				// Have no idea why I need to update it twice.
				// I hope I will find out soon.
				explosion.update(); explosion.update();
				return false;
			}
		}
		
		currentEnemy.update();
		
		if (isFlamed) {
			flame.update();

			if (flame.getAnimation().getCurrentFrameIndex()
				>= FLAME_RES_COUNT - 1) {
				isFlamed = false;
				flame.update();
			}
		}
		
		return true;
	}
	
	public void renderEnemy(Graphics2D g) {
		if (! isExploded) {
			currentEnemy.renderAnimation(g);
			if (isFlamed)
				flame.renderScaled(g);
		} else {
			explosion.renderScaled(g);
		}
	}
	
	public void burnEnemy() {		
		isFlamed = true;
	}
	
	public void explodeEnemy() {
		isExploded = true;
	}
	
	public VisualEffect getExplosion() {
		return explosion;
	}
	
	public Animation getExplosionAnimation() {
		return explosion.getAnimation();
	}
	
	public void setEnemiesPos() {
		for (Enemy enemy : enemies) {
			enemy.setPos(ENEMY_X, ENEMY_Y);
		}
	}
	
	public int getCurrentEnemiesHp() {
		return currentEnemiesHp;
	}

	public void setCurrentEnemiesHp(int currentEnemiesHp) {
		this.currentEnemiesHp = currentEnemiesHp;
		
		for (Enemy enemy : enemies) {
			enemy.setHp(currentEnemiesHp);
		}
	}

	public ArrayList<Enemy> getEnemies() {
		return enemies;
	}

	public void setEnemies(ArrayList<Enemy> enemies) {
		this.enemies = enemies;
	}

}
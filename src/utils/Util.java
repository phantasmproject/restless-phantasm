package utils;

import java.awt.Point;
import java.util.Random;

import main.GamePanel;

public class Util {
	
	public static int randNumInRange(int min, int max) {
		// In case of a mistake, switch min and max.
		if (min > max) {
			int tmp = max;
			max = min;
			min = tmp;
		}
		
		Random r = new Random();
		
		return r.nextInt((max - min) + 1) + min;
	}
	
	public static Point getCenter(double width, double height) {
		int centerX = (int) ((GamePanel.WIDTH / 2) - (width / 2));
		int centerY = (int) ((GamePanel.HEIGHT / 2) - (height / 2));
		
		return new Point(centerX, centerY); 
	}

}

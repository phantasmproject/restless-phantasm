package utils;

import javax.sound.sampled.*;

public class GameAudio {
	
	public static final String MENU_AUDIO_RES = "/audio/menu.wav";
	public static final String BUTTON_AUDIO_RES = "/audio/button_click.wav";
	public static final String GAMEPLAY_AUDIO_RES = "/audio/gameplay.wav";
	public static final String BOSS_FIGHT_AUDIO_RES = "/audio/boss_fight.wav";
	public static final String GAME_OVER_AUDIO_RES = "/audio/game_over.wav";
	public static final String BEAST_AUDIO_RES = "/audio/beast.wav";
	public static final String GHOST_AUDIO_RES = "/audio/ghost.wav";
	
	private Clip clip;
	
	public void playAudio(String audio_res) 
	{
	    try
	    {
	        clip = (Clip) AudioSystem.getLine(new Line.Info(Clip.class));

	        clip.addLineListener(new LineListener()
	        {
	            @Override
	            public void update(LineEvent event)
	            {
	                if (event.getType() == LineEvent.Type.STOP)
	                    clip.close();
	            }
	        });
	        
	        
	        clip.open(AudioSystem.getAudioInputStream
	        	(getClass().getResourceAsStream(audio_res)));
	        clip.start();
	    }
	    catch (Exception exc)
	    {
	        exc.printStackTrace(System.out);
	    }
	}
	
	public boolean isPlaying() {
		if (clip == null) return false;
		
		return clip.isRunning();
	}
	
	public void stopPlaying() {
		if (clip == null) return;
		
		clip.close();
	}
}
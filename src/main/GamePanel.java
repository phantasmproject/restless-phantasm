package main;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.*;

import states.GameState;

public class GamePanel extends JPanel implements ActionListener, KeyListener, MouseListener {
	private static final long serialVersionUID = 1L;
	
	private static final int FPS = 60;
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	private static final int SCALE = 1;
	
	private Timer gameTimer;
	private GameState currentState;
	private ArrayList<GameState> states;
	private BufferedImage mainImage;
	private Graphics2D g;
	
	public GamePanel() {
		super();
		
		setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		setFocusable(true);
		requestFocus();
		addKeyListener(this);
		addMouseListener(this);
		
		int speed = 1000 / FPS;
		gameTimer = new Timer(speed, this);
		
		states = new ArrayList<GameState>();
		
		mainImage = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		g = mainImage.createGraphics();
	}
	
	public void init() {
		for (GameState state: states)
			state.init();
	}
	
	public void run() {
		if (currentState == null) return;
	
		gameTimer.start();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		currentState.update();
		currentState.render(g);
		drawToScreen();
	}
	
	public void drawToScreen() {
		Graphics g = getGraphics();
		
		g.drawImage(mainImage, 0, 0, WIDTH * SCALE, HEIGHT * SCALE, null);
		g.dispose();
	}
	
	public void resetState(GameState newState) {
		newState.init();
		
		states.set(newState.getId(), newState);
	}
	
	public GameState getState(int stateIndex) {
		return states.get(stateIndex);
	}
	
	public GameState getStateById(int stateId) {
		for (GameState state : states) {
			if (state.getId() == stateId)
				return state;
		}
		
		return null;
	}
	
	public void setStateById(int stateId) {
		for (GameState state : states) {
			if (state.getId() == stateId)
				currentState = state;
		}
	}
	
	public void addState(GameState state) {
		this.states.add(state);
	}

	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void keyPressed(KeyEvent e) {
		currentState.keyPressed(e);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		currentState.keyReleased(e);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		currentState.mouseClicked(e);
	}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

}
package entities;

import java.awt.Graphics2D;

public class Player extends Sprite {
	
	private static final double SCALE = 0.5;
	private static final int BASE_HP = 5;
	private static final int BASE_ATK = 1;
	// private Animation animation;
	private int hp;
	private int atk;
	public boolean recentlyHit;
	
	public Player(String imgSrc) {
		super(imgSrc);
		hp = BASE_HP;
		atk = BASE_ATK;
		recentlyHit = false;
	}
	
	@Override
	public void update() {
		move();
	}

	@Override
	public void render(Graphics2D g) {
//		BufferedImage currentFrame = animation.getCurrentFrame();
//		g.drawImage(currentFrame, (int) x, (int) y, -currentFrame.getWidth(), currentFrame.getHeight(), null);
		
		g.drawImage(image, (int) x, (int) y, 
				(int) (image.getWidth() * SCALE), 
				(int) (image.getHeight() * SCALE), null);
	}
	
	public int getAtk() {
		return atk;
	}
	
	public void setAtk(int atk) {
		this.atk = atk;
	}
	
	public int getHp() {
		return hp;
	}
	
	public void resetHp() {
		hp = BASE_HP;
	}
	
	public void damaged(int dmg) {
		hp -= dmg;
		
		if (hp <= 0)
			isAlive = false;
	}
}